from decouple import config


TESTING = config('TESTING', default=False, cast=bool)
DEBUG = config('DEBUG', default=False, cast=bool)

''' Main API settings section'''
API_PREFIX = config('API_PREFIX', default='', cast=str)

''' Users settings section '''
SUSPENSION_TIME = config('SUSPENSION_TIME', default="1.days", cast=str)
SUSPENSION_MULTIPLIER = config('SUSPENSION_MULTIPLIER', default=2, cast=int)
CHANGE_EMAIL_ALLOWED = config('CHANGE_EMAIL_ALLOWED', default=True, cast=bool)
USER_MIN_PASSWORD_LENGTH = config('USER_MIN_PASSWORD_LENGTH', default=10, cast=int)

''' Database settings section '''
DB_URI = config('DB_URI', default=f"mongodb://localhost:27017/quest", cast=str)
TESTING_DB_URI = config('TESTING_DB_URI', default=f"mongodb://localhost:27017/testing", cast=str)
REDIS_URL = config('REDIS_URL', default="redis://localhost:6379", cast=str)

''' Security settings section '''
SECRET_KEY = config('SECRET_KEY', default='12345', cast=str)
GLOBAL_TOKEN_EXPIRATION = config('GLOBAL_TOKEN_EXPIRATION', default=12, cast=int) # Wrote in weeks
MIN_TOKEN_EXPIRATION = config('MIN_TOKEN_EXPIRATION', default=7, cast=int) # Wrote in days

UNAUTHORIZED_TRY_LIMIT = config('UNAUTHORIZED_TRY_LIMIT', default=3, cast=int)
UNAUTHORIZED_MAX_LIMIT = config('UNAUTHORIZED_TRY_LIMIT', default=5, cast=int)
UNAUTHORIZED_TRY_BAN_TIME = config('UNAUTHORIZED_TRY_TIME', default=60, cast=int)