from beanie import Document



class AsyncDocument(Document):

    async def onSeeding(self):
        pass
    
    async def update_document(self, data):
        iterable = data.__dict__.items() \
            if not isinstance(data, dict) \
                else data.items()
        for key, value in iterable:
            setattr(self, key, value)
        await super().replace()

