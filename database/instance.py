import logging
from types import MethodType
from importlib import import_module
from beanie import init_beanie, Document
from contextlib import asynccontextmanager
from motor.motor_asyncio import AsyncIOMotorClient

from lib.utils import load_modules
from lib.auth.models import Token, TokenBlacklist
import settings


class MongoDB:
    URI: str = None
    client: AsyncIOMotorClient = None
    models: list = []

    def __init__(self):
        super().__init__()
        self.URI = settings.DB_URI \
            if not settings.TESTING \
                else settings.TESTING_DB_URI

        logging.info(f"Database URI: {self.URI}")
        
    async def load_database_models(self):
        self.models += [ Token, TokenBlacklist ]
        
        for module in load_modules():
            try:
                models = import_module(f'modules.{module}.models')
                for obj in vars(models).values():
                    if isinstance(obj, type) and issubclass(obj, Document):
                        self.models.append(obj)
            except Exception as e:
                logging.error(f"Error loading models from module {module}: {e}")
                            
    async def connect(self, custom_uri: str = None):
        await self.load_database_models()

        self.client = AsyncIOMotorClient(self.URI if not custom_uri else custom_uri)
        
        database = self.client.get_default_database()
        await init_beanie(database=database, document_models=self.models)
        
        for model in self.models:
            bound = MethodType(model.onSeeding, model)
            await bound()
            
    async def disconnect(self):
        if self.client is not None:
            self.client.close()
   
    @asynccontextmanager
    async def lifespan(self, *args, **kwargs):
        await self.connect()
        yield
        await self.disconnect()
        