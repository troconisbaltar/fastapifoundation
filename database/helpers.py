from typing import Callable, Any, Dict
from beanie.operators import And
from beanie import Document


def parse_query_params(obj: Document, query_params: Dict[str, Any]):
    pipeline = []
    if len(query_params.keys()) > 1:
        for key, value in query_params.items():
            if value is not None:
                pipeline.append(getattr(obj, key) == value)
        return And(*pipeline) \
            if len(pipeline) > 1 \
                else getattr(obj, "id") != None
    else:
        if len(query_params):
            return getattr(obj, list(query_params.keys())[0]) == list(query_params.values())[0]
        return getattr(obj, "id") != None
        
def paginate_query(query_func: Callable[..., Any]) -> Callable[..., Any]:
    async def wrapper(*args, **kwargs) -> Dict[str, Any]:
        page = kwargs.pop("page", 1)
        page_size = kwargs.pop("page_size", 10)
        
        query = query_func(*args, **kwargs)
        if hasattr(query, '__await__'):
            query = await query
        skip = (page - 1) * page_size
        
        paginated_query = query.skip(skip).limit(page_size)
        return await paginated_query.to_list()
    return wrapper