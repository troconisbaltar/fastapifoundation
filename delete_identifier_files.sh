#!/bin/bash

# Buscar y eliminar recursivamente todos los archivos con la extensión .Identifier
find . -type f -name "*.Identifier" -exec rm -f {} \;

echo "Todos los archivos con la extensión .Identifier han sido eliminados."
