import logging
from fastapi import Depends, FastAPI, status
from importlib import import_module

from modules.user.permisions import permission_required, shared_state
from settings import API_PREFIX

from .state import *
from .utils import *



def index_routes(app: FastAPI):
    modules_under_permissions = []
    modules_under_permissions_map = {}
    
    total_routes = 0
    
    for module in load_modules():
        try:
            module_paths = import_module(f'modules.{module}.routes')
            
            parent_module = { 'parent': module.title(), 'modules': [] }
            for controller in module_paths.controllers:
                if controller_requires_permissions(controller):
                    module_loaded = extract_controller_name(controller)
                    module_loaded_router = controller.create_router()
                        
                    try:
                        permissions_required = controller.permissions_required
                    except AttributeError:
                        permissions_required = ['read', 'create', 'modify', 'delete']
                    
                    parent_module['modules'].append(
                        {'name': module_loaded, 
                         'permissions_required': permissions_required,
                         'routes': [{ 'path': route.path, 'methods': route.methods } \
                             for route in module_loaded_router.routes 
                            ]
                        }
                    )
                    
                module_router = controller.create_router()
                
                # Count total routes
                for route in module_router.routes:
                    total_routes += 1
                    
                app.include_router(prefix=API_PREFIX, router=module_router)
            
            if len(parent_module['modules']):
                modules_under_permissions.append(parent_module)
                
        except ModuleNotFoundError as e:
            pass
        
    logging.info(f"Routes loaded: {total_routes}")
    
    for parent in modules_under_permissions:
        for mup in parent['modules']:
            if len(mup) > 0:
                for route in mup['routes']:
                    modules_under_permissions_map[route['path']] = {
                        'module_name': mup['name'], 
                        'methods': route['methods'],
                        'permissions_required': mup['permissions_required']
                    } 

    shared_state.set_state(modules_under_permissions_map)
    
    modules_under_permissions_map['/modules/permissions/scheme'] = {
        'module_name': 'RetrieveModulesUnderPermissions', 
        'permissions_required': ['read']
    }
    
    @app.get("/modules/permissions/scheme", dependencies=[ Depends(permission_required) ])
    async def retrieve_modules_under_permissions():
        return { 
            "message": "Modules under permissions list retrieve success",
            "status_code": status.HTTP_200_OK,
            "data": modules_under_permissions 
        }