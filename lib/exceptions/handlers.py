from fastapi.exceptions import RequestValidationError, ResponseValidationError
from jwt.exceptions import InvalidSignatureError, DecodeError, ExpiredSignatureError
from pymongo.errors import DuplicateKeyError
from pydantic import ValidationError


from .utils import *


class CustomHandlers:
    
    server_exceptions = [
        ResponseValidationError,
        DuplicateKeyError,
    ]
    
    user_exceptions = [
        RequestValidationError,
        ValidationError,
    ]
    
    authentication_exceptions = [
        InvalidSignatureError,
        DecodeError,
        ExpiredSignatureError
    ]
    
    def __init__(self, app):
        self.app = app
        
    def add_handlers(self):
        # self.app.add_exception_handler(DoesNotExist, self.does_not_exist_handler)
        
        for exception in self.user_exceptions:
            self.app.add_exception_handler(exception, self.user_exception_handler)
        
        for exception in self.server_exceptions:
            self.app.add_exception_handler(exception, self.server_exception_handler)
            
        for exception in self.authentication_exceptions:
            self.app.add_exception_handler(exception, self.authentication_exception_handler)
    
    async def server_exception_handler(self, request, exc):
        try:
            input_data = await request.json()
        except:
            input_data = None
        return exception_response(500, exc, input_data)
    
    async def user_exception_handler(self, request, exc):
        try:
            input_data = await request.json()
        except:
            input_data = None
        return exception_response(400, exc, input_data)
        
    async def does_not_exist_handler(self, request, exc):
        return exception_response(404, exc)
    
    async def authentication_exception_handler(self, request, exc):
        return authentication_exception_response(401)