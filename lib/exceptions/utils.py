from fastapi.responses import JSONResponse


def exception_response(
    status_code: int, 
    exception: Exception, 
    input_data: dict = None
):
    return JSONResponse(
        status_code=status_code,
        content={
            "type": type(exception).__name__, 
            "details": str(exception),
            
            "input_data": input_data,
        },
    )

def authentication_exception_response(
    status_code: int, 
):
    return JSONResponse(
        status_code=status_code,
        content={
            "type": "AuthenticationError",
            "details": "Invalid token",
        },
    )