import aioredis
import contextvars

from fastapi import Request, Response
from fastapi.middleware.trustedhost import TrustedHostMiddleware
from uvicorn.middleware.proxy_headers import ProxyHeadersMiddleware
from fastapi.middleware.cors import CORSMiddleware

import settings


current_request: contextvars.ContextVar[Request] = contextvars.ContextVar("current_request")


class MiddlewareLoader:
    def __init__(self, *args, **kwds):
        self.redis_client = aioredis.from_url(
            url=settings.REDIS_URL, encoding="utf-8", decode_responses=True
        )

    async def request_context_middleware(self, request: Request, call_next):
        token = current_request.set(request)
        response = await call_next(request)
        current_request.reset(token)
        return response

    async def ban_many_unauthorized_middleware(self, request: Request, call_next):
        response = await call_next(request)
        
        if response.status_code == 401:
            client_ip = request.client.host
            
            attempts_key = f"attempts:{client_ip}"
            attempts = await self.redis_client.get(attempts_key)
            attempts = int(attempts) if attempts else 0
            
            if attempts >= settings.UNAUTHORIZED_MAX_LIMIT:
                ttl = settings.UNAUTHORIZED_TRY_BAN_TIME * (attempts + 1)
            else:
                ttl = settings.UNAUTHORIZED_TRY_BAN_TIME
                
            await self.redis_client.set(attempts_key, attempts + 1, ex=ttl)
            if attempts >= settings.UNAUTHORIZED_TRY_LIMIT:
                return Response(
                    status_code=403, 
                    content=f"Too many requests. Retry after {ttl} seconds"
                )
        return response

    async def check_ip_middleware(self, request: Request, call_next):
        client_ip = request.client.host
        attempts_key = f"attempts:{client_ip}"
        
        attempts = await self.redis_client.get(attempts_key)
        attempts = int(attempts) if attempts else 0
        
        if attempts >= settings.UNAUTHORIZED_MAX_LIMIT + 1:
            return Response(
                status_code=403, 
                content="IP banned, for too many requests. Wait"
            )
        return await call_next(request)

    def load(self, app):
        app.middleware("http")(self.ban_many_unauthorized_middleware)
        app.middleware("http")(self.request_context_middleware)
        app.middleware("http")(self.check_ip_middleware)
    
        app.add_middleware(ProxyHeadersMiddleware, trusted_hosts="*")
        app.add_middleware(TrustedHostMiddleware, allowed_hosts=["*"])
        app.add_middleware(CORSMiddleware, allow_origins=["*"], allow_credentials=True, allow_methods=["*"], allow_headers=["*"])