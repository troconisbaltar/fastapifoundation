from typing import List, Optional
from pydantic import BaseModel, model_validator

from database.model import AsyncDocument


class BaseRequest(BaseModel):

    @model_validator(mode="after")
    @classmethod
    def remove_null(cls, data):
        output = data.copy()
        
        for key, value in data.__dict__.items():
            if value is None:
                delattr(output, key)
        return output

class BaseFilters(BaseModel):
    page: Optional[int] = 1
    page_size: Optional[int] = 10


class BaseResponse(BaseModel):
    message: str
    status_code: int
    data: AsyncDocument = None
    error: dict = None
    
    @staticmethod
    def transform_id(data: dict) -> dict:
        if "_id" in data:
            return {'id': data.pop("_id"), **data}
        else:
            return data
    
    
class ManyResponse(BaseResponse):
    data: List[AsyncDocument] = []
