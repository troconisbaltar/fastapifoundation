import base64
from io import BytesIO
from PIL import Image


def is_valid_image(base64_string):
    try:
        if "data:image" in base64_string:
            base64_string = base64_string.split(",")[1]
        
        image_data = base64.b64decode(base64_string)
        image = BytesIO(image_data)

        img = Image.open(image)
        img.verify()
        return True
    except Exception as e:
        return False
    
def get_image_binary(base64_string):
    if "data:image" in base64_string:
        base64_string = base64_string.split(",")[1]
    
    image_data = base64.b64decode(base64_string)
    return image_data