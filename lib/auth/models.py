from datetime import datetime
from typing import Optional

from database.model import AsyncDocument


class Token(AsyncDocument):
    token: str
    user_id: str
    expiration_time: datetime
    renew_expiration_time: datetime
    last_used: Optional[datetime] = None
    
class TokenBlacklist(AsyncDocument):
    token: str
    expiration_time: datetime