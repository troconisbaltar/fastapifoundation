from importlib import import_module
import re


def controller_requires_permissions(controller):
    try:
        if "permission" in str(controller.dependencies):
            return True
        elif len(controller.permissions_required) > 0:
            return True
    except AttributeError:
        return False

def extract_controller_name(controller) -> str:
    chain = str(controller)
    
    patron = r"<class\s+'([^']+)'>"
    try:
        match = re.search(patron, chain)
    except TypeError:
        pass
    if match:
        classname = match.group(1)
        return classname.split('.')[-1].replace("Controller", "")
    return ""

def clean_form(form):
    __form = {}
    
    for k, v in form.items():
        if not v:
            continue
        __form[k] = v
    return __form

def load_modules():
    return import_module('modules').__all__

