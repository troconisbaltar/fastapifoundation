import httpx
import unittest

class BaseCase(unittest.TestCase):

    HEADERS = {
        "Content-Type": "application/json"
    }

    def setUp(self) -> None:
        client = httpx.Client(
            base_url="http://localhost:8000",
            headers=self.HEADERS
        )
        self.client = client

        return super().setUp()
    
    def tearDown(self) -> None:
        return super().tearDown()
    