import logging
from fastapi import FastAPI

from lib.routes import index_routes
from lib.exceptions.handlers import CustomHandlers
from lib.middlewares import MiddlewareLoader
from database.instance import MongoDB

import settings

# TODO: Implement filters in all retrieve routes
# TODO: Implement s3 buckets workflow for all images

logging.basicConfig(
    level = logging.INFO \
        if not settings.DEBUG \
            else logging.DEBUG
)


database_instance = MongoDB()

app = FastAPI(lifespan=database_instance.lifespan)
index_routes(app)

# Middlewares
middlewares = MiddlewareLoader()
middlewares.load(app)

exception_handlers = CustomHandlers(app)
exception_handlers.add_handlers()

