#!/usr/bin/env python

import logging
import argparse
import uvicorn

parser = argparse.ArgumentParser()
parser.add_argument('--testing', action='store_true')
parser.add_argument('--reload', action='store_true')
parser.add_argument('--debug', action='store_true')
parser.add_argument('--host', default='0.0.0.0', type=str)
parser.add_argument('--port', default=8000, type=int)
args = parser.parse_args()


if __name__ == "__main__":   
    if args.testing:
        import os
        os.environ['TESTING'] = 'True'
    logging.debug(os.environ.get('TESTING', 'False'))

    uvicorn.run("main:app", host=args.host, port=args.port, reload=args.reload)