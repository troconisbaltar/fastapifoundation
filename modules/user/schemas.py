from fastapi import HTTPException, status
from pydantic import BaseModel, EmailStr, model_validator, validator
from fastapi.exceptions import RequestValidationError
from typing import List, Optional
from datetime import datetime

from lib import schemas
from lib import images

from .models import *

import settings


class UserProfilePictureSchema(schemas.BaseRequest):
    profile_picture: str
    
    @validator('profile_picture')
    def is_valid(cls, v):
        if not images.is_valid_image(v):
            raise RequestValidationError('Invalid Image format')
        return v
    

class UserRequestSchema(UserProfilePictureSchema):
    name: str
    surnames: str
    username: Optional[str] = None
    email: EmailStr
    password: str
    profile_picture: Optional[str] = None
    
    @validator('password')
    def password_length(cls, v):
        if v != '':
            if len(v) < settings.USER_MIN_PASSWORD_LENGTH:
                raise RequestValidationError(
                    f'Password must be more than {settings.USER_MIN_PASSWORD_LENGTH} characters'
                )
        return v
    
    
class UserSelfUpdateSchema(UserRequestSchema, schemas.BaseRequest):
    name: Optional[str] = None
    surnames: Optional[str] = None
    username: Optional[str] = None
    password: Optional[str] = None
    email: Optional[EmailStr] = None
    roles: Optional[List[str]] = None
    user_permissions: Optional[List[str]] = None
    
    @validator('name', 'surnames', 'username', 'password', 'email', pre=True, always=True)
    def preprocess_data(cls, v):
        if isinstance(v, str):
            v = v.strip()
        return v
    
    @validator('email')
    def email_change_disabled(cls, v):
        if not settings.CHANGE_EMAIL_ALLOWED:
            raise RequestValidationError('Email cannot be changed')
        
    @validator('roles', 'user_permissions')
    def user_cannot_replace_roles(cls, v):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail='User cannot replace roles or permissions'
        )
        
        
class UserProfilePictureResponse(BaseModel):
    profile_picture: Optional[str] = None
    
    
class UserResponseSchema(schemas.BaseResponse):
    data: User
  
class UserLoginResponseSchema(UserResponseSchema):
    token: str

    
class UsersResponseSchema(schemas.ManyResponse):
    data: List[User]
    

class UserLoginRequestSchema(BaseModel):
    email_or_username: str
    password: str    
    

class NotificationSchema(BaseModel):
    user: str
    tree: Optional[str] = None
    notification_type: str
    content: str
    created_at: Optional[datetime] = None
    read: Optional[bool] = False

class ManagePermissionRequestSchema(BaseModel):
    fullcode: str
    
class GroupRequestSchema(BaseModel):
    name: str
    description: Optional[str]
    
class GroupRequestSchema(BaseModel):
    name: str
    description: Optional[str]
    
class GroupResponseSchema(schemas.BaseResponse):
    data: Group
        
class GroupsResponseSchema(schemas.ManyResponse):
    data: List[Group]

class RoleRequestSchema(GroupRequestSchema):
    ...
    
class RoleResponseSchema(schemas.BaseResponse):
    data: Role
    
class RolesResponseSchema(schemas.ManyResponse):
    data: List[Role]
    
class ReportRequestSchema(BaseModel):
    reason: str
    
class ReportUpdateRequestSchema(BaseModel):
    read: Optional[bool] = True
    is_open: Optional[bool] = True
    
class ReportResponseSchema(schemas.BaseResponse):
    data: Report
    
class ReportsResponseSchema(schemas.ManyResponse):
    data: List[Report]
    
class ReportsFilters(schemas.BaseFilters):
    reason: Optional[str] = None
    read: Optional[bool] = None
    is_open: Optional[bool] = None