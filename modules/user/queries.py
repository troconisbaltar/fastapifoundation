from json import loads
from datetime import datetime, timezone
from fastapi import HTTPException

from beanie.operators import Or, And

from database.helpers import paginate_query, parse_query_params

from .models import *
from .schemas import *

import settings


class UserQuery:
    
    @staticmethod
    async def get_users():
        return await User.find().to_list()

    @staticmethod
    async def retrieve_user(user_id: str):
        return await User.get(user_id)

    @staticmethod
    async def create_user(user: UserRequestSchema):
        data = dict(user)
        
        user = await User.find(
            Or(User.email == data["email"], User.username == data["username"])
        ).to_list()
        
        if user:
            raise HTTPException(status.HTTP_409_CONFLICT, 'User already exists')

        data['password'] = User.set_password(data.pop('password'))
        user = await User(**data).insert()
        return user
    
    @staticmethod
    async def login(user_schema: UserLoginRequestSchema):
        user = await User.find_one(
            Or(User.email == user_schema.email_or_username, 
               User.username == user_schema.email_or_username)
        )
        
        if not user or not await user.check_password(user_schema.password):
            return None
        
        user.last_login = datetime.now(timezone.utc)
        await user.replace()
        return user

    @staticmethod
    async def update_user(user: User, user_schema: UserSelfUpdateSchema):
        if "password" in user_schema.__dict__.keys():
            user_schema_copy = user_schema.model_copy()
            user_schema_copy.password = User.set_password(user_schema_copy.password)
            user_schema = user_schema_copy
        
        await user.update_document(user_schema)
        return user
    
    @staticmethod
    async def delete_profile_picture(user: User):
        user.profile_picture = None
        await user.replace()
        
    @staticmethod
    async def disable_user(user_id: str):
        user = await User.get(user_id)
        if not user:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'User not found')
        await user.update_document({"is_active": False})
        return user
    
    @staticmethod
    async def enable_user(user_id: str):
        user = await User.get(user_id)
        if not user:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'User not found')
        await user.update_document({"is_active": True})
        return user
        

class GroupQuery:
    
    @staticmethod
    async def get_groups():
        return await Group.find().to_list()
    
    @staticmethod
    async def retrieve_group(group_id: str):
        return await Group.get(group_id)
    
    @staticmethod
    async def retrieve_user_groups(user_id: str):
        user = await User.get(user_id)
        if not user:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'User not found')
        await user.fetch_all_links()
        return user.groups
    
    @staticmethod
    async def add_user_to_group(user_id: str, group_id: str):
        user = await User.get(user_id)
        if not user:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'User not found')
        group = await Group.get(group_id)
        if not group:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'Group not found')
        await user.fetch_all_links()
        if not group in user.groups:
            user.groups.append(group)
            await user.replace()
        else:
            raise HTTPException(status.HTTP_409_CONFLICT, 'This user already belongs to this group')
        return user
    
    @staticmethod
    async def remove_user_from_group(user_id: str, group_id: str):
        user = await User.get(user_id)
        if not user:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'User not found')
        group = await Group.get(group_id)
        if not group:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'Group not found')
        await user.fetch_all_links()
        if group in user.groups:
            user.groups.remove(group)
            await user.replace()
        else:
            raise HTTPException(status.HTTP_409_CONFLICT, 'This user does not belong to this group')
        return user
    
    @staticmethod
    async def create_group(group: GroupRequestSchema):
        data = dict(group)
        group = await Group.find_one(Group.name == data["name"])
        if group:
            raise HTTPException(status.HTTP_409_CONFLICT, 'Group already exists')
        group = await Group(**data).insert()
        return group
    
    @staticmethod
    async def update_group(group_id: str, group_schema: GroupRequestSchema):
        group = await Group.get(group_id)
        if not group:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'Group not found')
        await group.update_document(group_schema)
        return group
    
    @staticmethod
    async def delete_group(group_id: str):
        group = await Group.get(group_id)
        if not group:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'Group not found')
        await group.delete()
        

class RoleQuery:
    
    @staticmethod
    async def get_roles():
        return await Role.find().to_list()
    
    @staticmethod
    async def retrieve_role(role_id: str):
        return await Role.get(role_id)
    
    @staticmethod
    async def retrieve_user_roles(user_id: str):
        user = await User.get(user_id)
        if not user:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'User not found')
        await user.fetch_all_links()
        return user.roles
    
    @staticmethod
    async def create_role(role: RoleRequestSchema):
        data = dict(role)
        role = await Role.find_one(Role.name == data["name"])
        if role:
            raise HTTPException(status.HTTP_409_CONFLICT, 'Role already exists')
        role = await Role(**data).insert()
        return role
    
    @staticmethod
    async def update_role(role_id: str, role_schema: RoleRequestSchema):
        role = await Role.get(role_id)
        if not role:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'Role not found')
        await role.update_document(role_schema)
        return role
    
    @staticmethod
    async def delete_role(role_id: str):
        role = await Role.get(role_id)
        if not role:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'Role not found')
        await role.delete()
        
    @staticmethod
    async def assign_role(user_id: str, role_id: str):
        user = await User.get(user_id)
        if not user:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'User not found')
        
        role = await Role.get(role_id)
        if not role:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'Role not found')
        
        await user.fetch_all_links()
        if not role in user.roles:
            user.roles.append(role)
            await user.replace()
        else:
            raise HTTPException(status.HTTP_409_CONFLICT, 'This user already has this role')
        return user

    @staticmethod
    async def revoke_role(user_id: str, role_id: str):
        user = await User.get(user_id)
        if not user:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'User not found')
        
        role = await Role.get(role_id)
        if not role:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'Role not found')
        
        await user.fetch_all_links()
        if role in user.roles:
            user.roles.remove(role)
            await user.replace()
        else:
            raise HTTPException(status.HTTP_409_CONFLICT, 'This user does not have this role')
        return user
    

class PermissionQuery:

    @staticmethod
    async def get_object_permissions(Obj, obj_id: str):
        obj = await Obj.get(obj_id)
        if not obj:
            raise HTTPException(status.HTTP_404_NOT_FOUND, f'This ({Obj.__name__}) does not exist')
        return obj.permissions
    
    @staticmethod
    async def assign_permission(Obj, obj_id: str, permission: ManagePermissionRequestSchema):
        obj = await Obj.get(obj_id)
        
        if not obj:
            raise HTTPException(status.HTTP_404_NOT_FOUND, f'This ({Obj.__name__}) does not exist')
        
        if not permission.fullcode in obj.permissions:
            obj.permissions.append(permission.fullcode)
            await obj.replace()
        else:
            raise HTTPException(status.HTTP_409_CONFLICT, 'Permission already assigned')
        return obj

    @staticmethod
    async def revoke_permission(Obj, obj_id: str, permission: ManagePermissionRequestSchema):
        obj = await Obj.get(obj_id)
        
        if not obj:
            raise HTTPException(status.HTTP_404_NOT_FOUND, f'This ({Obj.__name__}) does not exist')
        
        if permission.fullcode in obj.permissions:
            obj.permissions.remove(permission.fullcode)
            await obj.replace()
        else:
            raise HTTPException(status.HTTP_409_CONFLICT, f'The object ({Obj.__name__}) does not have this permission assigned')
        return obj
    
    
class SuspendUserQuery:
    
    @staticmethod
    async def check_suspension(user: User):
        suspension = await Suspension.find_one(
            And(Suspension.user.id == user.id,
                Suspension.expires_at > datetime.now(),
                Suspension.active == True)
        )
        return suspension
    
    @staticmethod
    async def suspend_user(user_id: str):
        user = await User.get(user_id)
        if not user:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'User not found')
        
        has_active_suspension = await Suspension.find_one(
            And(Suspension.user.id == user.id, 
                Suspension.expires_at > datetime.now(), 
                Suspension.active == True)
        )
        
        if has_active_suspension:
            raise HTTPException(
                409, 
                f'User has an active suspension, remains: {has_active_suspension.expires_at - datetime.now()}'
            )
        
        # Retrieve record last record
        last_suspension = await Suspension.find(
            Suspension.user.id == user.id
        ).sort(-Suspension.created_at).first_or_none()
        
        if last_suspension:
            suspensions_count = await Suspension.find(Suspension.user.id == user.id).count()
            if suspensions_count >= 2:
                await user.update_document({"is_active": False})
                raise HTTPException(status.HTTP_409_CONFLICT, 'User has reached the maximum number of suspensions, user has been disabled')
            
            last_suspension_duration = last_suspension.expires_at - last_suspension.created_at
            new_suspension_duration = last_suspension_duration * settings.SUSPENSION_MULTIPLIER
            expires_at = datetime.now() + new_suspension_duration
            suspension = await Suspension(user=user, expires_at=expires_at).insert()
            
        else:
            suspension = await Suspension(user=user).insert()
            
        suspension_time = suspension.expires_at - datetime.now()
        return suspension_time
    
    @staticmethod
    async def unsuspend_user(user_id: str):
        user = await User.get(user_id)
        if not user:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'User not found')
        
        active_suspension = await Suspension.find_one(
            And(Suspension.user.id == user.id, 
                Suspension.expires_at > datetime.now(),
                Suspension.active == True)
        )
        
        if not active_suspension:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'User does not have an active suspension')
        
        await active_suspension.update_document({"active": False})
        return active_suspension
    

class ReportQuery:
    
    @staticmethod
    async def get_user_reports(user_id: str):
        reported_user = await User.get(user_id)
        if not reported_user:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'Reported user not found')
        reports = await Report.find(
            Report.reported_user.id == reported_user.id, 
            fetch_links=True
        ).sort(-Report.created_at).to_list()
        
        return reports
    
    @staticmethod
    @paginate_query
    def get_users_reports(*args, **kwargs):
        return Report.find(
            parse_query_params(Report, kwargs),
            fetch_links=True
        ).sort(-Report.created_at)
    
    @staticmethod
    async def create_report(user: User, user_id: str, report_data: ReportRequestSchema):
        data = dict(report_data)
        
        reported_user = await User.get(user_id)
        if not reported_user:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'Reported user not found')
        
        user_reports = await Report.find_one(
            And(Report.user.id == user.id,
                Report.reported_user.id == reported_user.id,
                Report.is_open == True)
        )
        
        if user_reports:
            raise HTTPException(status.HTTP_409_CONFLICT, 'User has already reported this user')
        
        report = await Report(user=user, reported_user=reported_user, **data).insert()
        await report.fetch_all_links()
        
        return report
    
    @staticmethod
    async def update_report(report_id: str, report_data: ReportUpdateRequestSchema):
        data = dict(report_data)
        report = await Report.get(report_id)
        if not report:
            raise HTTPException(status.HTTP_404_NOT_FOUND, 'Report not found')
        
        data['reviewed_at'] = datetime.now()
        await report.update_document(data)
        await report.fetch_all_links()
        
        return report