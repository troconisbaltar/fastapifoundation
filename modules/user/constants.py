

# NOTIFICATIONS_CHOICES = [
#     'message',
#     'new_follower',
#     'new_many_followers'
# ]

ROLE_PERMISSIONS = {
    "admin": {
        "Users.write",
        "Users.modify",
        "Users.delete",
        
        "RetrieveUsersList.read",
        
        "RetrieveModulesUnderPermissions.read",
        "RetrieveAllExistingPermissionsController.read",
        
        "AssignUserPermissionController.modify",
        "RevokeUserPermissionController.modify",
        
        "RetrieveUserGroupsController.read",
        "RetrieveExistingUserGroupsController.read",
        "UsersGroupManagementController.write",
        "UsersGroupManagementController.modify",
        "UsersGroupManagementController.delete",
        "AddUserToGroupController.write"
        "RemoveUserFromGroupController.write"
        
        "RetrieveUsersRolesController.read",
        "RetrieveExistingUserRolesController.read",
        "UserRolespManagementController.write",
        "UserRolespManagementController.modify",
        "UserRolespManagementController.delete",
        "AssignUserRoleController.write"
        "RevokeUserRoleController.write"
        
        "EnableUserController.write",
        "DisableUserController.write",
        
        "UserSuspensionManagementController.write",
        "UserSuspensionManagementController.delete",
        
        "UsersReportsManagementController.read"
    },
    "user": {
    },
}