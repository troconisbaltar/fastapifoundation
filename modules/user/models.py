import bcrypt
from typing import List, Optional
from datetime import datetime, timedelta
from beanie import Indexed, Insert, Link, before_event
from pydantic import Field, EmailStr

from database.model import AsyncDocument

import settings

from . import constants
from . import utils
    
    
class Group(AsyncDocument):
    name: str = Indexed(unique=True)
    description: Optional[str]
    
    permissions: Optional[List[str]] = []
    created_at: datetime = Field(default_factory=datetime.now)
    updated_at: datetime = Field(default_factory=datetime.now)
    
    class Settings:
        use_cache = True
        cache_expiration_time = timedelta(seconds=10)
        cache_capacity = 5
    

class Role(AsyncDocument):
    name: str = Indexed(unique=True)
    description: Optional[str]
    
    permissions: Optional[List[str]] = []
    created_at: datetime = Field(default_factory=datetime.now)
    updated_at: datetime = Field(default_factory=datetime.now)
    
    class Settings:
        use_cache = True
        cache_expiration_time = timedelta(seconds=10)
        cache_capacity = 5
        
    
class User(AsyncDocument):
    name: str
    surnames: str
    username: Optional[str] = Indexed(unique=True)
    email: EmailStr = Indexed(unique=True)
    password: str = Field(exclude=True, min_length=settings.USER_MIN_PASSWORD_LENGTH)
    profile_picture: Optional[str] = Field(exclude=True, default=None)
    is_active: bool = Field(default=False)
    is_private: bool = Field(default=False)
    
    permissions: Optional[List[str]] = Field(exclude=True, default_factory=list)
    roles: Optional[List[Link[Role]]] = Field(exclude=True, default_factory=list)
    groups: Optional[List[Link[Group]]] = Field(exclude=True, default_factory=list)
    
    created_at: datetime = Field(default_factory=datetime.now)
    updated_at: datetime = Field(default_factory=datetime.now)
    last_login: Optional[datetime] = None
    
    @before_event(Insert)
    def normalize_fields(self):
        self.name = self.name.capitalize()
        self.surnames = self.surnames.capitalize()
        self.email = self.email.lower()
   
    @staticmethod 
    def set_password(password: str) -> str:
        return bcrypt.hashpw(
            password.encode('utf-8'), 
            bcrypt.gensalt()
        ).decode('utf-8')
        
    def save(self, *args, **kwargs) -> AsyncDocument:
        password = kwargs.pop("password", None)
        if password:
            self.set_password(password)            
        return super().insert(*args, **kwargs)
    
    async def check_password(self, password: str, *args, **kwargs) -> bool:
        return bcrypt.checkpw(
            password.encode('utf-8'), 
            self.password.encode('utf-8')
        )

    @classmethod
    async def onSeeding(cls, *args, **kwargs):      
        if not await cls.find_one():
            group, role, user_role = None, None, None
            
            try:
                group = await Group.find_one(name="staff")
                role = await Role.find_one(name="admin")
                user_role = await Role.find_one(name="user")
                
            except:
                if not group and not role and not user_role:
                    group = await Group(
                        name="staff", 
                        description="This is the staff group"
                    ).insert()
                    
                    role = await Role(
                        name="admin", 
                        description="This is the admin role",
                        permissions=[ perm \
                            for perm in constants.ROLE_PERMISSIONS["admin"]
                        ]
                    ).insert()
                    
                    user_role = await Role(
                        name="user", 
                        description="This is the user role"
                    ).insert()
                
                await cls(
                    name="System",
                    surnames="Admin",
                    username="admin",
                    email="admin@system.com",
                    password=cls.set_password("password"), 
                    is_private=True,
                    groups=[group],
                    roles=[role, user_role]
                ).insert()
            

class Report(AsyncDocument):
    user: Link[User]
    reported_user: Link[User]
    reason: str
    created_at: datetime = Field(default_factory=datetime.now)
    reviewed_at: Optional[datetime] = None
    read: bool = Field(default=False)
    is_open: bool = Field(default=True)


class Suspension(AsyncDocument):
    user: Link[User]
    expires_at: datetime = Field(default_factory=utils.get_suspension_time)
    created_at: datetime = Field(default_factory=datetime.now)
    report: Optional[Link[Report]] = None
    reason: Optional[str] = None
    active: bool = Field(default=True)
    
    
# class Notification(AsyncDocument):
#     user = mge.ReferenceField(User, required=True)
#     notification_type = mge.StringField(required=True, choices=constants.NOTIFICATIONS_CHOICES)
#     content = mge.StringField(required=True)
#     created_at = mge.DateTimeField(default=datetime.now(timezone.utc))
#     read = mge.BooleanField(default=False)
    
    

    

