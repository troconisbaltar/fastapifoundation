from datetime import datetime, timedelta
from faker import Faker

import settings


faker = Faker()

def generate_register_payload() -> dict:
    return {
        "fullname": faker.name(),
        "username": faker.user_name(),
        "email": faker.email(),
        "password": faker.password()
    }
    
def generate_login_payload(previous_data: dict) -> dict:
    return {
        "email_or_username": previous_data["email"],
        "password": previous_data["password"]
    }
    
def generate_fake_base64_image() -> str:
    return "iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=="

def get_suspension_time() -> datetime:
    return datetime.now() + \
            timedelta(**{part.split('.')[1]: int(part.split('.')[0]) \
                for part in [settings.SUSPENSION_TIME]}
            )