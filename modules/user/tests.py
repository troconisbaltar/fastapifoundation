from random import choice
from lib import testing

from .utils import *


class UserRegisterTest(testing.BaseCase):
    endpoint = "/user/register"
    
    def test_register_success(self):
        response = self.client.post(self.endpoint, json=generate_register_payload())
        self.assertEqual(201, response.status_code)
        
    def test_too_short_password_validation(self):
        payload = generate_register_payload()
        payload['password'] = '123'
        response = self.client.post(self.endpoint, json=payload)
        self.assertEqual(400, response.status_code)

    def test_empty_payload_validation(self):
        response = self.client.post(self.endpoint, json={})
        self.assertEqual(400, response.status_code)
        
    def test_unknown_field_validation(self):
        response = self.client.post(self.endpoint, json={'unknown_field': 'value'})
        self.assertEqual(400, response.status_code)

class UserLoginTest(testing.BaseCase):
    REGISTERING_PAYLOAD = generate_register_payload()
    LOGIN_PAYLOAD = generate_login_payload(previous_data=REGISTERING_PAYLOAD)
    
    def test_login(self):
        self.client.post("/user/register", json=self.REGISTERING_PAYLOAD)
        
        login_response = self.client.post("/auth/login", json=self.LOGIN_PAYLOAD)
        self.assertEqual(200, login_response.status_code)
        
    def test_bad_credentials_validation(self):
        self.client.post("/user/register", json=self.REGISTERING_PAYLOAD)
        
        payload = self.LOGIN_PAYLOAD.copy()
        payload['password'] = 'wrong_password'
        
        login_response = self.client.post("/auth/login", json=payload)
        self.assertEqual(401, login_response.status_code)
                
class UserLogoutTest(testing.BaseCase):
    REGISTERING_PAYLOAD = generate_register_payload()
    LOGIN_PAYLOAD = generate_login_payload(previous_data=REGISTERING_PAYLOAD)
    
    def test_logout(self):
        self.client.post("/user/register", json=self.REGISTERING_PAYLOAD)
        
        login_response = self.client.post("/auth/login", json=self.LOGIN_PAYLOAD)
        login_response_data = login_response.json()
        
        token = login_response_data["token"]
        self.client.headers["Authorization"] = f"Bearer {token}"
        logout_response = self.client.post( "/auth/logout")
        self.assertEqual(200, logout_response.status_code)
        
class GetLoggedInUserInformationTest(testing.BaseCase):
    REGISTERING_PAYLOAD = generate_register_payload()
    LOGIN_PAYLOAD = generate_login_payload(previous_data=REGISTERING_PAYLOAD)
    
    def test_retrieve(self):
        registering_response = self.client.post("/user/register", json=self.REGISTERING_PAYLOAD)
        self.assertEqual(201, registering_response.status_code)
        
        login_response = self.client.post("/auth/login", json=self.LOGIN_PAYLOAD)
        self.assertEqual(200, login_response.status_code)
        login_response_data = login_response.json()
        
        token = login_response_data["token"]
        self.client.headers["Authorization"] = f"Bearer {token}"
        
        user_response = self.client.get("/user/me")
        self.assertEqual(200, user_response.status_code)
    
class UpdateLoggedInUserInformationTest(testing.BaseCase):
    REGISTERING_PAYLOAD = generate_register_payload()
    LOGIN_PAYLOAD = generate_login_payload(previous_data=REGISTERING_PAYLOAD)
    
    def test_update(self):
        self.client.post("/user/register", json=self.REGISTERING_PAYLOAD)
        
        login_response = self.client.post("/auth/login", json=self.LOGIN_PAYLOAD)
        login_response_data = login_response.json()
        
        token = login_response_data["token"]
        self.client.headers["Authorization"] = f"Bearer {token}"
        
        user_response = self.client.put("/user/me", json=self.REGISTERING_PAYLOAD)
        self.assertEqual(200, user_response.status_code)
        
class RetrieveLoggedInUserProfilePictureTest(testing.BaseCase):
    REGISTERING_PAYLOAD = generate_register_payload()
    LOGIN_PAYLOAD = generate_login_payload(previous_data=REGISTERING_PAYLOAD)
    
    def test_retrieve(self):
        self.client.post("/user/register", json=self.REGISTERING_PAYLOAD)
        
        login_response = self.client.post("/auth/login", json=self.LOGIN_PAYLOAD)
        login_response_data = login_response.json()
        
        token = login_response_data["token"]
        self.client.headers["Authorization"] = f"Bearer {token}"
        
        self.client.put("/user/profilePicture/me", json={'profile_picture': generate_fake_base64_image()})
        profile_picture_response = self.client.get("/user/profilePicture/me")
        self.assertEqual(200, profile_picture_response.status_code)
    
    def test_not_found_validation(self):
        self.client.post("/user/register", json=self.REGISTERING_PAYLOAD)
        
        login_response = self.client.post("/auth/login", json=self.LOGIN_PAYLOAD)
        login_response_data = login_response.json()
        
        token = login_response_data["token"]
        self.client.headers["Authorization"] = f"Bearer {token}"
        
        profile_picture_response = self.client.get("/user/profilePicture/me")
        self.assertEqual(404, profile_picture_response.status_code)
        
class RetrieveLoggedInUserProfilePictureBinaryTest(testing.BaseCase):
    REGISTERING_PAYLOAD = generate_register_payload()
    LOGIN_PAYLOAD = generate_login_payload(previous_data=REGISTERING_PAYLOAD)
    
    def test_retrieve(self):
        self.client.post("/user/register", json=self.REGISTERING_PAYLOAD)
        
        login_response = self.client.post("/auth/login", json=self.LOGIN_PAYLOAD)
        login_response_data = login_response.json()
        
        token = login_response_data["token"]
        self.client.headers["Authorization"] = f"Bearer {token}"
        
        self.client.put("/user/profilePicture/me", json={'profile_picture': generate_fake_base64_image()})
        profile_picture_response = self.client.get("/user/profilePicture/binary/me")
        self.assertEqual(200, profile_picture_response.status_code)
        self.assertEqual("image/png", profile_picture_response.headers["Content-Type"])
        self.assertEqual(type(profile_picture_response.read()), bytes)
    
    def test_not_found_validation(self):
        self.client.post("/user/register", json=self.REGISTERING_PAYLOAD)
        
        login_response = self.client.post("/auth/login", json=self.LOGIN_PAYLOAD)
        login_response_data = login_response.json()
        
        token = login_response_data["token"]
        self.client.headers["Authorization"] = f"Bearer {token}"
        
        profile_picture_response = self.client.get("/user/profilePicture/me")
        self.assertEqual(404, profile_picture_response.status_code)
        
class UpdateLoggedInUserProfilePictureTest(testing.BaseCase):
    REGISTERING_PAYLOAD = generate_register_payload()
    LOGIN_PAYLOAD = generate_login_payload(previous_data=REGISTERING_PAYLOAD)
    
    def test_update(self):
        self.client.post("/user/register", json=self.REGISTERING_PAYLOAD)
        
        login_response = self.client.post("/auth/login", json=self.LOGIN_PAYLOAD)
        login_response_data = login_response.json()
        
        token = login_response_data["token"]
        self.client.headers["Authorization"] = f"Bearer {token}"
        
        profile_picture_response = self.client.put("/user/profilePicture/me", json={'profile_picture': generate_fake_base64_image()})
        self.assertEqual(200, profile_picture_response.status_code)
    
    def test_bad_format_validation(self):
        self.client.post("/user/register", json=self.REGISTERING_PAYLOAD)
        
        login_response = self.client.post("/auth/login", json=self.LOGIN_PAYLOAD)
        login_response_data = login_response.json()
        
        token = login_response_data["token"]
        self.client.headers["Authorization"] = f"Bearer {token}"
        
        profile_picture_response = self.client.put("/user/profilePicture/me", json={'profile_picture': 'profile_picture'})
        self.assertEqual(400, profile_picture_response.status_code)
        
    def test_unknown_field_validation(self):
        self.client.post("/user/register", json=self.REGISTERING_PAYLOAD)
        
        login_response = self.client.post("/auth/login", json=self.LOGIN_PAYLOAD)
        login_response_data = login_response.json()
        
        token = login_response_data["token"]
        self.client.headers["Authorization"] = f"Bearer {token}"
        
        profile_picture_response = self.client.put("/user/profilePicture/me", json={'unknown_field': 'profile_picture'})
        self.assertEqual(400, profile_picture_response.status_code)
        
class DeleteLoggedInUserProfilePictureTest(testing.BaseCase):
    REGISTERING_PAYLOAD = generate_register_payload()
    LOGIN_PAYLOAD = generate_login_payload(previous_data=REGISTERING_PAYLOAD)
    
    def test_delete(self):
        self.client.post("/user/register", json=self.REGISTERING_PAYLOAD)
        
        login_response = self.client.post("/auth/login", json=self.LOGIN_PAYLOAD)
        login_response_data = login_response.json()
        
        token = login_response_data["token"]
        self.client.headers["Authorization"] = f"Bearer {token}"
        
        profile_picture_response = self.client.put("/user/profilePicture/me", json={'profile_picture': generate_fake_base64_image()})
        
        profile_picture_response = self.client.delete("/user/profilePicture/me")
        self.assertEqual(204, profile_picture_response.status_code)
    
    def test_not_found_validation(self):
        self.client.post("/user/register", json=self.REGISTERING_PAYLOAD)
        
        login_response = self.client.post("/auth/login", json=self.LOGIN_PAYLOAD)
        login_response_data = login_response.json()
        
        token = login_response_data["token"]
        self.client.headers["Authorization"] = f"Bearer {token}"
        
        profile_picture_response = self.client.get("/user/profilePicture/me")
        self.assertEqual(404, profile_picture_response.status_code)
        
# ADMIN ENDPOINTS
        
class GetProtectedEndpointsWithPermissionsTest(testing.BaseCase):
    LOGIN_PAYLOAD = {
        "email_or_username": "admin",
        "password": "password"
    }
    
    def test_retrieve(self):
        login_response = self.client.post("/auth/login", json=self.LOGIN_PAYLOAD)
        login_response_data = login_response.json()
        
        token = login_response_data["token"]
        self.client.headers["Authorization"] = f"Bearer {token}"
        
        permissions_response = self.client.get("/modules/permissions/scheme")
        self.assertEqual(200, permissions_response.status_code)
        
        
class AssignPermissionsToUserTest(testing.BaseCase):
    LOGIN_PAYLOAD = {
        "email_or_username": "admin",
        "password": "password"
    }
    
    def test_assign(self):
        
        login_response = self.client.post("/auth/login", json=self.LOGIN_PAYLOAD)
        login_response_data = login_response.json()
        user_id = login_response_data['data']['_id']
        
        token = login_response_data["token"]
        self.client.headers["Authorization"] = f"Bearer {token}"
        
        permissions_response = self.client.get("/modules/permissions/scheme")
        self.assertEqual(200, permissions_response.status_code)
        
        # List of permissions to check
        permissions_to_check = []
        
        # Add available permissions
        for permission in permissions_response.json()['data']:
            for module in permission['modules']:
                for permission_required in module['permissions_required']:
                    fullcode = f"{module['name']}.{permission_required}"
                    permission_response = self.client.post(f"/permission/assign/user/{user_id}", json={'fullcode': fullcode})            
                    self.assertEqual(200, permission_response.status_code)
                    
                    permissions_to_check.append(fullcode)
                    
        # Get user information
        user_info_response = self.client.get("/user/me")
        for permission in permissions_to_check:
            self.assertIn(permission, user_info_response.json()['data']['permissions'])
            
    
class RevokePermissionsToUserTest(testing.BaseCase):
    LOGIN_PAYLOAD = {
        "email_or_username": "admin",
        "password": "password"
    }
    
    def test_revoke(self):        
        login_response = self.client.post("/auth/login", json=self.LOGIN_PAYLOAD)
        login_response_data = login_response.json()
        
        user_id = login_response_data['data']['_id']
        
        token = login_response_data["token"]
        self.client.headers["Authorization"] = f"Bearer {token}"
        
        permissions_response = self.client.get("/modules/permissions/scheme")
        self.assertEqual(200, permissions_response.status_code)
        
        permissions_data = permissions_response.json()['data']
        random_permission = choice(permissions_data)
        random_permission = choice(random_permission['modules'])
        
        fullcode = f"{random_permission['name']}.{choice(random_permission['permissions_required'])}"
        
        permission_revoke_response = self.client.put(f"/permission/revoke/user/{user_id}", json={'fullcode': fullcode})            
        self.assertEqual(200, permission_revoke_response.status_code)