from bson import ObjectId
from fastapi import status
from fastapi import Depends
from fastapi import HTTPException

from lib.state import SharedState
from lib.middlewares import current_request

from .auth import token_authentication  


shared_state = SharedState()

METHODS_MAP = {
    'GET': 'read',
    'POST': 'create',
    'PUT': 'modify',
    'DELETE': 'delete'
}


def parse_endpoint(request):
    url_path = str(request.url).split('/')
    for i, part in enumerate(url_path):
        if ObjectId.is_valid(part):
            url_path[i] = "{" + f"{url_path[i - 1]}_id" + "}"
    
    full_url = "/".join(url_path)
    if "?" in full_url:
        full_url = full_url.split("?")[0]
    return f"/{full_url.replace(str(request.base_url), '')}"

async def permission_required(user = Depends(token_authentication)):
    user_enabled_to = set()
    
    permissions_types_required = []
    module_name = None
    
    try:
        request = current_request.get()
        if not module_name:
            endpoint = parse_endpoint(request)
            module_name = shared_state.state[endpoint]['module_name']
        
            if not len(permissions_types_required):
                permissions_types_required = shared_state.state[endpoint]['permissions_required']
        http_method = request.method
        
    except KeyError:
        raise HTTPException(
            status_code=400, 
            detail=f"The endpoint: '{endpoint}' isn't mapped yet, contact an administrator"
        )
    
    except LookupError:
        raise HTTPException(
            status_code=500, 
            detail="Request content not found"
        )

    await user.fetch_all_links()
    
    # 1. Permissions by user roles (using the constant)
    if user.roles:
        if "admin" in [role.name for role in user.roles]:
            return user
        
        for role in user.roles:
            user_enabled_to.update(role.permissions)
        
        # 2. Assigned permisions to the user
        if user.permissions:
            user_enabled_to.update(user.permissions)
        
        # 3. User permisions through the groups
        if user.groups:
            for group in user.groups:
                if group.permissions:
                    user_enabled_to.update(group.permissions)
        
        # Whole code or search (by example "UsersController.read")
        if len(permissions_types_required):
            permission_requested = METHODS_MAP[http_method]
            if permission_requested not in permissions_types_required:
                return user
            
            for required_permission_type in permissions_types_required:
                full_permission = (f"{module_name}.{required_permission_type}")
                    
                if full_permission not in user_enabled_to:
                    raise HTTPException(
                        status_code=status.HTTP_403_FORBIDDEN,
                        detail=f"Access denied: REQUIRED '{full_permission}'"
                    )
        else:
            required_permission_type = METHODS_MAP[http_method]
            full_permission = f"{module_name}.{required_permission_type}"
                    
            if full_permission not in user_enabled_to:
                raise HTTPException(
                    status_code=status.HTTP_403_FORBIDDEN,
                    detail=f"Access denied: REQUIRED '{full_permission}'"
                )       
    else:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="User doesn't have any role, contact a system admin"
        )
    return user