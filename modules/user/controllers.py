from fastapi import Depends, Response, status, HTTPException
from fastapi_controllers import Controller
from fastapi_controllers import get, post, put, delete

from lib import images

from .schemas import *
from .queries import *
from .permisions import permission_required

from . import auth


class UserRegisterController(Controller):
    
    prefix = '/user'

    @post('/register', response_model=UserResponseSchema, status_code=status.HTTP_201_CREATED)
    async def register_user(self, user_schema: UserRequestSchema):
        user = await UserQuery.create_user(user_schema)
        return {
            'message': 'User created successfully', 
            'status_code': status.HTTP_201_CREATED, 
            'data': user
        }
        
class UserLoginController(Controller):
    
    prefix = '/auth'
    
    @post('/login', response_model=UserLoginResponseSchema)
    async def login_user(self, user_schema: UserLoginRequestSchema):
        user = await UserQuery.login(user_schema)
        
        if not user:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail='Invalid credentials'
            )
        elif not user.is_active:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail='User is not active'
            )
        elif await SuspendUserQuery.check_suspension(user):
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail='User is suspended'
            )
        
        token = await auth.token_session(user_id=str(user.id))
        return {
            'message': 'User logged in successfully',
            'status_code': status.HTTP_200_OK,
            'data': user,
            'token': token 
        }
        
    @post('/logout')
    async def logout_user(self, _: User = Depends(auth.destroy_token)):
        return {
            'message': 'User logout success',
            'status_code': status.HTTP_200_OK,
        }

        
class LoggedUserInteractionsController(Controller):
    
    prefix = '/user/me'
    dependencies = [ Depends(auth.token_authentication) ]
    
    @get('', response_model=UserResponseSchema)
    async def get_logged_user(self, user: User = Depends(auth.token_authentication)):
        return {
            'message': 'User retrieved successfully',
            'status_code': status.HTTP_200_OK,
            'data': user
        }
        
    @put('', response_model=UserResponseSchema)
    async def update_user(self, user: User = Depends(auth.token_authentication), user_schema: UserSelfUpdateSchema = None):
        user = await UserQuery.update_user(user, user_schema)
        return {
            'message': 'User updated successfully',
            'status_code': status.HTTP_200_OK,
            'data': user
        }
        

class GetLoggedUserProfilePictureBinaryController(Controller):
        
    prefix = '/user/me/profilePicture/binary'
    dependencies = [ Depends(auth.token_authentication) ]
        
    @get('')
    async def get_user_profile_picture(self, user: User = Depends(auth.token_authentication)):
        if not user.profile_picture:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Profile picture not found."
            )
        image = images.get_image_binary(user.profile_picture)
        return Response(content=image, media_type="image/png")
    
    
class GetLoggedUserProfilePictureController(Controller):
    
    prefix = '/user/me/profilePicture'
    dependencies = [ Depends(auth.token_authentication) ]
    
    @get('', response_model=UserProfilePictureResponse)
    async def get_user_profile_picture(self, user: User = Depends(auth.token_authentication)):
        if not user.profile_picture:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Profile picture not found."
            )
        profile_picture = user.profile_picture
        return {
            'profile_picture': profile_picture
        }
    
    @put('', response_model=UserResponseSchema)
    async def update_user_profile_picture(self, user: User = Depends(auth.token_authentication), user_schema: UserProfilePictureSchema = None):
        user = await UserQuery.update_user(user, user_schema)
        return {
            'message': 'User profile picture updated successfully',
            'status_code': status.HTTP_200_OK,
            'data': user
        }
        
    @delete('')
    async def delete_user_profile_picture(self, user: User = Depends(auth.token_authentication)):
        if not user.profile_picture:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="Profile picture not found."
            )
        await UserQuery.delete_profile_picture(user)
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    
    
class RetrieveUsersListController(Controller):
    
    prefix = '/users'
    permissions_required = ["read"]
    dependencies = [ Depends(permission_required) ]

    @get('', response_model=UsersResponseSchema)
    async def get_users(self):
        users = await UserQuery.get_users()
        return {
            'message': 'Usuarios obtenidos correctamente',
            'status_code': status.HTTP_200_OK,
            'data': users
        }
    
    
class RetrieveUserPermissionsController(Controller):
    
    prefix = '/user'
    permissions_required = ["read"]
    dependencies = [ Depends(permission_required) ]

    @get('/{user_id}/permissions', response_model=schemas.ManyResponse)
    async def get_user_permissions(self, user_id: str):
        permissions = await PermissionQuery.get_object_permissions(User, user_id)
        return {
            'message': 'Permissions retrieved successfully',
            'status_code': status.HTTP_200_OK,
            'data': permissions
        }


class AssignUserPermissionController(Controller):
    
    prefix = '/permission/assign'
    permissions_required = ["modify"]
    dependencies = [ Depends(permission_required) ]

    @post('/user/{user_id}', response_model=schemas.BaseResponse)
    async def user_asign_permission(self, user_id: str, permission: ManagePermissionRequestSchema):
        permission = await PermissionQuery.assign_permission(User, user_id, permission)
        return {
            "message": "Permission updated succesfully", 
            "status_code": status.HTTP_200_OK,
            "data": permission
        }
    
    @post('/group/{group_id}', response_model=schemas.BaseResponse)
    async def group_asign_permission(self, group_id: str, permission: ManagePermissionRequestSchema):
        permission = await PermissionQuery.assign_permission(Group, group_id, permission)
        return {
            "message": "Permission updated succesfully", 
            "status_code": status.HTTP_200_OK,
            "data": permission
        }
        
    @post('/role/{role_id}', response_model=schemas.BaseResponse)
    async def role_asign_permission(self, role_id: str, permission: ManagePermissionRequestSchema):
        permission = await PermissionQuery.assign_permission(Role, role_id, permission)
        return {
            "message": "Permission updated succesfully", 
            "status_code": status.HTTP_200_OK,
            "data": permission
        }
        
        
class RevokePermissionController(Controller):
    
    prefix = '/permission/revoke'
    permissions_required = ["modify"]
    dependencies = [ Depends(permission_required) ]

    @post('/user/{user_id}', response_model=schemas.BaseResponse)
    async def revoke_permission(self, user_id: str, permission: ManagePermissionRequestSchema):
        permission = await PermissionQuery.revoke_permission(User, user_id, permission)
        return {
            "message": "Permission removed succesfully", 
            "status_code": status.HTTP_200_OK
        }
        
    @post('/group/{group_id}', response_model=schemas.BaseResponse)
    async def revoke_permission(self, group_id: str, permission: ManagePermissionRequestSchema):
        permission = await PermissionQuery.revoke_permission(Group, group_id, permission)
        return {
            "message": "Permission removed succesfully", 
            "status_code": status.HTTP_200_OK
        }
        
    @post('/role/{role_id}', response_model=schemas.BaseResponse)
    async def revoke_permission(self, role_id: str, permission: ManagePermissionRequestSchema):
        permission = await PermissionQuery.revoke_permission(Role, role_id, permission)
        return {
            "message": "Permission removed succesfully", 
            "status_code": status.HTTP_200_OK
        }


class RetrieveExistingUserGroupsController(Controller):
    
    prefix = '/users/groups'
    permissions_required = ["read"]
    dependencies = [ Depends(permission_required) ]
    
    @get('', response_model=GroupsResponseSchema)
    async def get_user_groups(self):
        user_groups = await GroupQuery.get_groups()
        return {
            'message': 'User groups retrieved successfully',
            'status_code': status.HTTP_200_OK,
            'data': user_groups
        }
        
        
class RetrieveUsersGroupsController(Controller):
    
    prefix = '/user'
    permissions_required = ["read"]
    dependencies = [ Depends(permission_required) ]
    
    @get('/{user_id}/groups', response_model=GroupsResponseSchema)
    async def get_user_groups(self, user_id: str):
        user_groups = await GroupQuery.retrieve_user_groups(user_id)
        return {
            'message': 'User groups retrieved successfully',
            'status_code': status.HTTP_200_OK,
            'data': user_groups
        }
        
        
class UsersGroupManagementController(Controller):
    
    prefix = '/users/group'
    permissions_required = ["write", "modify", "delete"]
    dependencies = [ Depends(permission_required) ]
    
    @post('', response_model=GroupResponseSchema)
    async def create_group(self, group: GroupRequestSchema):
        group = await GroupQuery.create_group(group)
        return {
            'message': 'Group created successfully',
            'status_code': status.HTTP_201_CREATED,
            'data': group
        }
        
    @put('/{group_id}', response_model=GroupResponseSchema)
    async def modify_group(self, group_id: str, group: GroupRequestSchema):
        group = await GroupQuery.update_group(group_id, group)
        return {
            'message': 'Group updated successfully',
            'status_code': status.HTTP_201_CREATED,
            'data': group
        }
        
    @delete('/{group_id}', status_code=status.HTTP_204_NO_CONTENT)
    async def delete_group(self, group_id: str):
        await GroupQuery.delete_group(group_id)
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    

class AddUserToGroupController(Controller):
    
    prefix = '/user'
    permissions_required = ["write"]
    dependencies = [ Depends(permission_required) ]
    
    @post('/{user_id}/group/{group_id}/add', response_model=schemas.BaseResponse)
    async def add_user_to_group(self, user_id: str, group_id: str):
        user = await GroupQuery.add_user_to_group(user_id, group_id)
        return {
            'message': 'User added to group successfully',
            'status_code': status.HTTP_200_OK
        }
        
        
class RemoveUserFromGroupController(Controller):
    
    prefix = '/user'
    permissions_required = ["write"]
    dependencies = [ Depends(permission_required) ]
    
    @post('/{user_id}/group/{group_id}/remove', response_model=schemas.BaseResponse)
    async def remove_user_from_group(self, user_id: str, group_id: str):
        user = await GroupQuery.remove_user_from_group(user_id, group_id)
        return {
            'message': 'User removed from group successfully',
            'status_code': status.HTTP_200_OK
        }
    

class RetrieveUserRolesController(Controller):
    
    prefix = '/user'
    permissions_required = ["read"]
    dependencies = [ Depends(permission_required) ]
    
    @get('/{user_id}/roles', response_model=RolesResponseSchema)
    async def get_user_roles(self, user_id: str):
        user_roles = await RoleQuery.retrieve_user_roles(user_id)
        return {
            'message': 'User roles retrieved successfully',
            'status_code': status.HTTP_200_OK,
            'data': user_roles
        }
    
class RetrieveExistingUserRolesController(Controller):
    
    prefix = '/users/roles'
    permissions_required = ["read"]
    dependencies = [ Depends(permission_required) ]
    
    @get('', response_model=RolesResponseSchema)
    async def get_user_roles(self):
        user_roles = await RoleQuery.get_roles()
        return {
            'message': 'User roles retrieved successfully',
            'status_code': status.HTTP_200_OK,
            'data': user_roles
        }
        
        
class UserRolesManagementController(Controller):
    
    prefix = '/users/role'
    permissions_required = ["write", "modify", "delete"]
    dependencies = [ Depends(permission_required) ]
    
    @post('', response_model=RoleResponseSchema)
    async def create_role(self, role: RoleRequestSchema):
        role = await RoleQuery.create_role(role)
        return {
            'message': 'Role created successfully',
            'status_code': status.HTTP_201_CREATED,
            'data': role
        }
        
    @put('/{role_id}', response_model=RoleResponseSchema)
    async def modify_role(self, role_id: str, role: RoleRequestSchema):
        role = await RoleQuery.update_role(role_id, role)
        return {
            'message': 'Role updated successfully',
            'status_code': status.HTTP_201_CREATED,
            'data': role
        }
        
    @delete('/{role_id}', status_code=status.HTTP_204_NO_CONTENT)
    async def delete_role(self, role_id: str):
        await RoleQuery.delete_role(role_id)
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    
    
class AssignUserRoleController(Controller):
    
    prefix = '/user'
    permissions_required = ["write"]
    dependencies = [ Depends(permission_required) ]
    
    @post('/{user_id}/role/{role_id}/assign', response_model=schemas.BaseResponse)
    async def role_asign_permission(self, user_id: str, role_id: str):
        permission = await RoleQuery.assign_role(user_id, role_id)
        return {
            "message": "Role asigned successfully", 
            "status_code": status.HTTP_200_OK,
            "data": permission
        }
    
class RevokeUserRoleController(Controller):
    
    prefix = '/user'
    permissions_required = ["write"]
    dependencies = [ Depends(permission_required) ]
    
    @post('/{user_id}/role/{role_id}/revoke', response_model=schemas.BaseResponse)
    async def role_revoke_permission(self, user_id: str, role_id: str):
        permission = await RoleQuery.revoke_role(user_id, role_id)
        return {
            "message": "Role revoked successfully", 
            "status_code": status.HTTP_200_OK,
            "data": permission
        }
    
class DisableUserController(Controller):
    
    prefix = '/user'
    permissions_required = ["write"]
    dependencies = [ Depends(permission_required) ]
    
    @post('/{user_id}/disable', response_model=schemas.BaseResponse)
    async def disable_user(self, user_id: str):
        user = await UserQuery.disable_user(user_id)
        return {
            'message': f'User ({user.id}) has been disabled',
            'status_code': status.HTTP_201_CREATED
        }

class EnableUserController(Controller):
    
    prefix = '/user'
    permissions_required = ["write"]
    dependencies = [ Depends(permission_required) ]
    
    @post('/{user_id}/enable', response_model=schemas.BaseResponse)
    async def enable_user(self, user_id: str):
        user = await UserQuery.enable_user(user_id)
        return {
            'message': f'User ({user.id}) has been enabled',
            'status_code': status.HTTP_201_CREATED
        }
        
class UserSuspensionManagementController(Controller):
    
    prefix = '/user'
    permissions_required = ["write", "delete"]
    dependencies = [ Depends(permission_required) ]
    
    @post('/{user_id}/suspend', response_model=schemas.BaseResponse)
    async def suspend_user(self, user_id: str):
        suspension_time = await SuspendUserQuery.suspend_user(user_id)
        return {
            'message': f'User ({user_id}) has been suspended for {suspension_time}',
            'status_code': status.HTTP_201_CREATED
        }
    
    @post('/{user_id}/unsuspend', response_model=schemas.BaseResponse)
    async def unsuspend_user(self, user_id: str):
        await SuspendUserQuery.unsuspend_user(user_id)
        return {
            'message': f'User ({user_id}) has been unsuspended',
            'status_code': status.HTTP_201_CREATED
        }
        
        
class ReportUserController(Controller):
    
    prefix = '/user'
    
    @post('/{user_id}/report', response_model=ReportResponseSchema)
    async def create_report(self, user_id: str, report: ReportRequestSchema, user: User = Depends(auth.token_authentication)):
        report = await ReportQuery.create_report(user, user_id, report)
        return {
            'message': 'Report created successfully',
            'status_code': status.HTTP_201_CREATED,
            'data': report
        }
        
        
class UsersReportsRetrieveController(Controller):
    
    prefix = '/users/reports'
    permissions_required = ["read"]
    dependencies = [ Depends(permission_required) ]
    
    @get('', response_model=ReportsResponseSchema)
    async def get_users_reports(self, filters: ReportsFilters = Depends()):
        reports = await ReportQuery.get_users_reports(**dict(filters))
        return {
            'message': 'Users reports retrieved successfully',
            'status_code': status.HTTP_200_OK,
            'data': reports
        }
        
        
class UserReportManagementController(Controller):
    
    prefix = '/user'
    permissions_required = ["read", "modify"]
    dependencies = [ Depends(permission_required) ]
    
    @get('/{user_id}/reports', response_model=ReportsResponseSchema)
    async def get_user_reports(self, user_id: str):
        reports = await ReportQuery.get_user_reports(user_id)
        return {
            'message': 'User reports retrieved successfully',
            'status_code': status.HTTP_200_OK,
            'data': reports
        }
    
    @put('/report/{report_id}', response_model=ReportResponseSchema)
    async def update_report(self, report_id: str, report: ReportUpdateRequestSchema):
        report = await ReportQuery.update_report(report_id, report)
        return {
            'message': 'Report updated successfully',
            'status_code': status.HTTP_200_OK,
            'data': report
        }