from .controllers import *

controllers = [
    UserRegisterController,
    RetrieveUsersListController,
    
    UserLoginController,
    
    LoggedUserInteractionsController,
    GetLoggedUserProfilePictureController,
    GetLoggedUserProfilePictureBinaryController,
    
    RetrieveUserPermissionsController,
    AssignUserPermissionController,
    RevokePermissionController,
    
    RetrieveUsersGroupsController,
    RetrieveExistingUserGroupsController,
    UsersGroupManagementController,
    AddUserToGroupController,
    RemoveUserFromGroupController,
    
    RetrieveUserRolesController,
    RetrieveExistingUserRolesController,
    UserRolesManagementController,
    AssignUserRoleController,
    RevokeUserRoleController,
    
    EnableUserController,
    DisableUserController,
    
    UserSuspensionManagementController,
    
    ReportUserController,
    UsersReportsRetrieveController,
    UserReportManagementController
]