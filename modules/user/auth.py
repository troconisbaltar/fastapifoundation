import jwt
from uuid import uuid4
from datetime import datetime, timedelta

from fastapi import HTTPException, Security
from fastapi.security import OAuth2PasswordBearer
from beanie.operators import And

from lib.auth.models import Token, TokenBlacklist

import settings

from .models import *

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/auth/login")


def generate_uuid():
    return uuid4().hex[: 11]

async def store_token(
    token: str, 
    user_id: str, 
    expiration_time: datetime, 
    renew_expiration_time: datetime
):
    stored_token = Token(
        token = token, 
        user_id = user_id, 
        expiration_time = expiration_time,
        renew_expiration_time = renew_expiration_time
    )
    await stored_token.insert()
    
    return stored_token

async def token_session(user_id: str):
    exp = datetime.now() + timedelta(weeks=settings.GLOBAL_TOKEN_EXPIRATION)
    renew_expiration = datetime.now() + timedelta(days=settings.MIN_TOKEN_EXPIRATION)
    
    token = jwt.encode({'user_id': user_id, 'exp': exp},
                      settings.SECRET_KEY, algorithm='HS256')
    
    await store_token(
        token = token,
        user_id = user_id,
        expiration_time = exp,
        renew_expiration_time = renew_expiration
    )
    
    return token

def token_define(account_id):
    string = generate_uuid()
    return jwt.encode({'account_id': account_id, 'string': string},
                      settings.SECRET_KEY, algorithm='HS256')
    
def token_check(token):
    return jwt.decode(token, settings.SECRET_KEY, algorithms='HS256')

async def destroy_token(token: str = Security(oauth2_scheme)):
    token_check(token)
    
    blackisted = await TokenBlacklist.find_one(TokenBlacklist.token == token)
    if blackisted:
        raise HTTPException(401, "You can't destroy an invalid token")
    
    token_object = await Token.find_one(Token.token == token)
    token_blacklist = TokenBlacklist(
        token = token,
        expiration_time = token_object.expiration_time
    )
    
    await token_blacklist.insert()
    return 

async def check_suspension(user: User):
    suspension = await Suspension.find_one(
        And(Suspension.user.id == user.id,
            Suspension.expires_at > datetime.now(),
            Suspension.active == True)
    )
    return suspension

async def token_authentication(token: str = Security(oauth2_scheme)):
    token_data = token_check(token)
    
    blackisted = await TokenBlacklist.find_one(TokenBlacklist.token == token)
    if blackisted:
        raise HTTPException(401, 'Token is expired')
    
    token_object = await Token.find_one(Token.token == token)

    if not token_object:
        token_object = await store_token(
            token, 
            token_data['user_id'], 
            datetime.fromtimestamp(token_data['exp']),
            datetime.now() + timedelta(days=settings.MIN_TOKEN_EXPIRATION)
        )
        
    now = datetime.now()
    until = token_object.renew_expiration_time - now
    
    if until < timedelta(days=settings.MIN_TOKEN_EXPIRATION) \
        and until > timedelta(days=0):
        token_object.renew_expiration_time = now + timedelta(days=settings.MIN_TOKEN_EXPIRATION)
        await token_object.save()
        
    elif until < timedelta(days=0):
        token_object.delete()
        
        token_blacklist = TokenBlacklist(
            token = token,
            expiration_time = token_object.expiration_time
        )
        await token_blacklist.insert()
        
        raise HTTPException(403, 'Token expired')
    
    user_id = token_data['user_id']
    user = await User.get(user_id)

    if not user:
        raise HTTPException(401, 'Token invalid')
    
    if not user.is_active:
        raise HTTPException(403, 'Your account is disabled')
    
    # We'll check if the user has a suspension
    suspension = await check_suspension(user)
    if suspension:
        raise HTTPException(403, 'Your account has been suspended')
    
    return user