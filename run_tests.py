#!/usr/bin/env python

import unittest

test_loader = unittest.TestLoader()
test_suite = test_loader.discover('modules', pattern='tests.py')

test_runner = unittest.TextTestRunner(stream=None, descriptions=False, verbosity=2)
test_runner.run(test_suite)